# Github Page

[![Build Status](https://travis-ci.org/renaudaste/github-page.svg?branch=master)](https://travis-ci.org/renaudaste/github-page)


This project is the code of my [Github page ](https://renaudaste.github.io/)

It was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.30.

## Stack
- Angular 2
- angular-cli
- yarn
- angular-material
- angular-cli-ghpages

## Versions
Currently working on my machine with :
- nvm with node version 7.5.0
- angular-cli 1.0.0-beta.30
- yarn 0.19.1

## Deploy
Run:
```sh
ng build --prod
angular-cli-ghpages --repo=git@github.com:renaudaste/renaudaste.github.io.git --branch=master
```
## Links
- angular-cli : https://github.com/angular/angular-cli
- angular-material : https://github.com/angular/material2
- angular-cli-ghpages : https://github.com/angular-buch/angular-cli-ghpages
- yarn : https://yarnpkg.com/lang/en/docs/install/

## Help
- Help for angular material : https://material.angular.io/

## More

Comming soon...
